package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp2 {
	
	public static void main(String[] args) throws Exception
	{
		
		Class.forName("com.mysql.jdbc.Driver");//oracle.jdbc.driver.OracleDriver
		
		//jdbc:oracle:@localhost:1521:xe, system, password
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/zensar", "root", "password");
		/*
		PreparedStatement pst = con.prepareStatement("insert into book values(?,?,?)");
		
		pst.setInt(1, 3);
		pst.setString(2, "Spring");
		pst.setString(3, "RJ");
		
		pst.execute();
		*/
		
		
		PreparedStatement pst = con.prepareStatement("update book set author = ? where id =?");
		
		pst.setString(1, "James");
		pst.setInt(2, 3);
		
		pst.execute();
		
		PreparedStatement pst1 = con.prepareStatement("select * from book");
		
		ResultSet rs = pst1.executeQuery();
		
		while(rs.next()) {
		
		System.out.println("ID : "+rs.getInt(1)+"Name : "+rs.getString(2)+" Author : "+rs.getString(3));
		}
		
		con.close();
	}

}

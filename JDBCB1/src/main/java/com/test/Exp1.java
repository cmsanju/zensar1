package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Exp1 {
	
	public static void main(String[] args) throws Exception
	{
		//step 1
		Class.forName("com.mysql.jdbc.Driver");
		
		//step 2
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/zensar", "root", "password");
		
		con.setAutoCommit(false);
		
		//step 3
		Statement stmt = con.createStatement();
		
		//String sql = "create table book (id int, name varchar(50), author varchar(50))";
		
		String sql2 = "insert into book values(2, 'Adv Java', 'GMS')";
		
		//String author = "Gosling";
		
		String sql = "update book set author = 'Green team' where id = 1 ";
		
		stmt.addBatch(sql2);
		stmt.addBatch(sql);
		
		stmt.executeBatch();
		
		//con.commit();
		
		con.rollback();
		
		String sql1 = "select * from book";
		
		//step 4
		stmt.execute(sql);
		
		ResultSet rs = stmt.executeQuery(sql1);
		
		while(rs.next()) {
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" Author : "+rs.getString(3));
		}
		
		//step 5 close connection object
		con.close();
	}

}
